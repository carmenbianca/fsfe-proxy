FROM nginx:alpine

EXPOSE 80

COPY proxy.conf /etc/nginx/conf.d/
COPY setdest.sh /app/

CMD /app/setdest.sh && nginx -g "daemon off;"
